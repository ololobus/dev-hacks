# Windows

### Disk performance test

```shell
winsat disk -drive DISK_LETTER
```
where `DISK_LETTER` is in `c/d/g/f/etc.`

### Kill all processes by name

```shell
taskkill /F /IM program_name.exe
```
